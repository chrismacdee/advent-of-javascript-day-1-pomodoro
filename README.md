# pomodoro-timer
Pomodoro timer for advent of js day 1.

Progress board - https://trello.com/b/4KaFyqKC

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
